/* eslint-disable import/prefer-default-export */
const toggleShowMixin = {
  data() {
    return {
      show: false
    };
  },
  methods: {
    toggleShow() {
      this.show = !this.show;
    }
  }
};

export { toggleShowMixin };
